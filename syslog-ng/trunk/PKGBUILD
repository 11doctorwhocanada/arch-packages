# $Id$
# Maintainer: Eric Bélanger <eric@archlinux.org>

pkgname=syslog-ng
pkgver=3.6.2
pkgrel=1
pkgdesc="Next-generation syslogd with advanced networking and filtering capabilities"
arch=('i686' 'x86_64')
url="http://www.balabit.com/network-security/syslog-ng/"
license=('GPL2' 'LGPL2.1')
depends=('awk' 'eventlog' 'systemd' 'pcre' 'libdbi')
makedepends=('python2' 'libxslt' )
optdepends=('logrotate: for rotating log files')
backup=('etc/syslog-ng/scl.conf'
        'etc/syslog-ng/syslog-ng.conf'
        'etc/logrotate.d/syslog-ng')
source=(http://www.balabit.com/downloads/files/syslog-ng/sources/$pkgver/source/${pkgname}_$pkgver.tar.gz
        syslog-ng.conf syslog-ng.logrotate syslog-ng-fix-test.diff)
sha1sums=('36eeaf920383ee8cb1c17d945205b7562f9fbbb2'
          '3e7ec4f3f68265aaa98f37338f801c5c22b85c17'
          '949128fe3d7f77a7aab99048061f885bc758000c'
          'ee0eb0a03a78bbdad8492e6ac8a7330fd46cd0e7')

prepare() {
  cd $pkgname-$pkgver
  sed -i -e 's,/bin/,/usr/bin/,' -e 's,/sbin/,/bin/,' contrib/systemd/syslog-ng.service
  patch -p1 -i "$srcdir/syslog-ng-fix-test.diff"
}

build() {
  cd $pkgname-$pkgver
  ./configure --prefix=/usr --sysconfdir=/etc/syslog-ng --libexecdir=/usr/lib \
    --sbindir=/usr/bin --localstatedir=/var/lib/syslog-ng --datadir=/usr/share/syslog-ng \
    --with-pidfile-dir=/run --disable-spoof-source --enable-ipv6 --enable-sql \
    --enable-systemd --with-systemdsystemunitdir=/usr/lib/systemd/system
  make
}

check() {
  cd $pkgname-$pkgver
  make check
}

package() {
  make -C $pkgname-$pkgver DESTDIR="$pkgdir" install
  install -dm755 "$pkgdir/var/lib/syslog-ng" "$pkgdir/etc/syslog-ng/patterndb.d"
  install -Dm644 "$srcdir/syslog-ng.conf" "$pkgdir/etc/syslog-ng/syslog-ng.conf"
  install -Dm644 "$srcdir/syslog-ng.logrotate" "$pkgdir/etc/logrotate.d/syslog-ng"
}
