# $Id$
# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>
# Contributor: Pierre Schmitz <pierre@archlinux.de>

pkgbase=kde-meta
pkgname=('kde-meta-kdeaccessibility'
         'kde-meta-kdeadmin'
         'kde-meta-kdeartwork'
         'kde-meta-kdebase'
         'kde-meta-kdeedu'
         'kde-meta-kdegames'
         'kde-meta-kdegraphics'
         'kde-meta-kdemultimedia'
         'kde-meta-kdenetwork'
         'kde-meta-kdepim'
         'kde-meta-kdesdk'
         'kde-meta-kdetoys'
         'kde-meta-kdeutils'
         'kde-meta-kdewebdev')
pkgver=14.12
pkgrel=1
arch=('any')
url='http://www.archlinux.org/'
license=('GPL')
groups=('kde-meta')

package_kde-meta-kdeaccessibility() {
	pkgdesc=('Meta package for kdeaccessibility')
	depends=('kdeaccessibility-jovie'
             'kdeaccessibility-kaccessible'
	         'kdeaccessibility-kmag'
	         'kdeaccessibility-kmousetool'
	         'kdeaccessibility-kmouth')
	replaces=('kdeaccessibility')
	conflicts=('kdeaccessibility')
}

package_kde-meta-kdeadmin() {
	pkgdesc=('Meta package for kdeadmin')
	depends=('kdeadmin-kcron'
	         'kdeadmin-ksystemlog'
             'kdeadmin-kuser')
	replaces=('kdeadmin')
	conflicts=('kdeadmin')
}

package_kde-meta-kdeartwork() {
	pkgdesc=('Meta package for kdeartwork')
	depends=('kdeartwork-colorschemes'
	         'kdeartwork-desktopthemes'
	         'kdeartwork-emoticons'
	         'kdeartwork-iconthemes'
	         'kdeartwork-kscreensaver'
	         'kdeartwork-styles'
	         'kdeartwork-wallpapers'
	         'kdeartwork-weatherwallpapers')
	replaces=('kdeartwork')
	conflicts=('kdeartwork')
}

package_kde-meta-kdebase() {
	pkgdesc=('Meta package for kdebase')
	depends=('kdebase-dolphin'
	         'kdebase-kdepasswd'
	         'kdebase-kdialog'
	         'kdebase-kfind'
             'kdebase-keditbookmarks'
	         'kdebase-konqueror'
             'kdebase-konq-plugins'
	         'konsole'
	         'kwrite'
	         'kdebase-plasma'
           'kde-wallpapers'
           'kdebase-workspace')
	replaces=('kdebase')
	conflicts=('kdebase')
}

package_kde-meta-kdeedu() {
	pkgdesc=('Meta package for kdeedu')
	depends=('kdeedu-artikulate'
           'kdeedu-blinken'
	         'kdeedu-cantor'
	         'kalgebra'
	         'kdeedu-kalzium'
	         'kanagram'
	         'kdeedu-kbruch'
	         'kdeedu-kgeography'
	         'khangman'
	         'kig'
	         'kdeedu-kiten'
	         'kdeedu-klettres'
	         'kdeedu-kmplot'
	         'kdeedu-kstars'
	         'kdeedu-ktouch'
	         'kdeedu-kturtle'
	         'kdeedu-kwordquiz'
	         'kdeedu-marble'
             'kdeedu-pairs'
	         'parley'
	         'kdeedu-rocs'
	         'kdeedu-step')
	replaces=('kdeedu')
	conflicts=('kdeedu')
}

package_kde-meta-kdegames() {
	pkgdesc=('Meta package for kdegames')
	depends=('kdegames-bomber'
	         'kdegames-bovo'
	         'kdegames-granatier'
	         'kdegames-kajongg'
	         'kdegames-kapman'
	         'kdegames-katomic'
	         'kdegames-kblackbox'
	         'kdegames-kblocks'
	         'kdegames-kbounce'
	         'kdegames-kbreakout'
	         'kdegames-kdiamond'
	         'kdegames-kfourinline'
	         'kdegames-kgoldrunner'
	         'kdegames-kigo'
	         'kdegames-killbots'
	         'kdegames-kiriki'
	         'kdegames-kjumpingcube'
	         'kdegames-klines'
	         'kdegames-klickety'
	         'kdegames-kmahjongg'
	         'kdegames-kmines'
	         'kdegames-knavalbattle' 
             'kdegames-knetwalk'
	         'kdegames-kolf'
	         'kdegames-kollision'
	         'kdegames-konquest'
	         'kdegames-kpatience'
	         'kdegames-kreversi'
	         'kdegames-kshisen'
	         'kdegames-ksirk'
             'kdegames-ksnakeduel'
	         'kdegames-kspaceduel'
	         'kdegames-ksquares'
	         'kdegames-ksudoku'
	         'kdegames-ktuberling'
	         'kdegames-kubrick'
	         'kdegames-lskat'
	         'kdegames-palapeli'
             'kdegames-picmi')
	replaces=('kdegames')
	conflicts=('kdegames')
}

package_kde-meta-kdegraphics() {
	pkgdesc=('Meta package for kdegraphics')
	depends=('gwenview'
	         'kdegraphics-kamera'
	         'kdegraphics-kcolorchooser'
	         'kdegraphics-kgamma'
	         'kdegraphics-kolourpaint'
	         'kdegraphics-kruler'
             'kdegraphics-ksaneplugin'
	         'kdegraphics-ksnapshot'
             'kdegraphics-mobipocket'
	         'kdegraphics-okular'
             'kdegraphics-strigi-analyzer'
             'kdegraphics-svgpart'
             'kdegraphics-thumbnailers')
	replaces=('kdegraphics')
	conflicts=('kdegraphics')
}

package_kde-meta-kdemultimedia() {
	pkgdesc=('Meta package for kdemultimedia')
	depends=('kdemultimedia-audiocd-kio'
             'kdemultimedia-dragonplayer'
	         'kdemultimedia-ffmpegthumbs'
	         'kdemultimedia-juk'
	         'kdemultimedia-kmix'
	         'kdemultimedia-kscd'
	         'kdemultimedia-mplayerthumbs')
	replaces=('kdemultimedia')
	conflicts=('kdemultimedia')
}

package_kde-meta-kdenetwork() {
	pkgdesc=('Meta package for kdenetwork')
	depends=('kdenetwork-filesharing'
	         'kdenetwork-kget'
	         'kdenetwork-kopete'
	         'kdenetwork-kppp'
	         'kdenetwork-krdc'
	         'kdenetwork-krfb'
           'kdenetwork-strigi-analyzers'
           'kdenetwork-zeroconf-ioslave')
	replaces=('kdenetwork')
	conflicts=('kdenetwork')
}

package_kde-meta-kdepim() {
	pkgdesc=('Meta package for kdepim')
	depends=('kdepim-akonadiconsole'
	         'kdepim-akregator'
	         'kdepim-blogilo'
	         'kdepim-console'
	         'kdepim-kaddressbook'
	         'kdepim-kalarm'
	         'kdepim-kjots'
	         'kdepim-kleopatra'
	         'kdepim-kmail'
	         'kdepim-knode'
	         'kdepim-knotes'
	         'kdepim-kontact'
	         'kdepim-korganizer'
	         'kdepim-kresources'
	         'kdepim-ktimetracker'
             'kdepim-ktnef')
	replaces=('kdepim')
	conflicts=('kdepim')
}

package_kde-meta-kdesdk() {
	pkgdesc=('Meta package for kdesdk')
	depends=('kdesdk-cervisia'
	         'kdesdk-dolphin-plugins'
	         'kdesdk-dev-scripts'
             'kdesdk-dev-utils'
             'kapptemplate'
	         'kate'
	         'kdesdk-kcachegrind'
	         'kdesdk-kioslaves'
	         'kdesdk-kompare'
	         'kdesdk-lokalize'
	         'okteta'
	         'kdesdk-poxml'
	         'kdesdk-strigi-analyzers'
	         'kdesdk-thumbnailers'
	         'kdesdk-umbrello')
	replaces=('kdesdk')
	conflicts=('kdesdk')
}

package_kde-meta-kdetoys() {
	pkgdesc=('Meta package for kdetoys')
	depends=('kdetoys-amor'
	         'kdetoys-kteatime'
	         'kdetoys-ktux')
	replaces=('kdetoys')
	conflicts=('kdetoys')
}

package_kde-meta-kdeutils() {
	pkgdesc=('Meta package for kdeutils')
	depends=('kdeutils-ark'
	         'kdeutils-filelight'
	         'kdeutils-kcalc'
	         'kdeutils-kcharselect'
	         'kdeutils-kdf'
	         'kdeutils-kfloppy'
	         'kdeutils-kgpg'
	         'kdeutils-kremotecontrol'
	         'kdeutils-ktimer'
	         'kdeutils-kwalletmanager'
	         'kdeutils-print-manager'
	         'kdeutils-superkaramba'
	         'kdeutils-sweeper')
	replaces=('kdeutils')
	conflicts=('kdeutils')
}

package_kde-meta-kdewebdev() {
	pkgdesc=('Meta package for kdewebdev')
	depends=('kdewebdev-kfilereplace'
	         'kdewebdev-kimagemapeditor'
	         'kdewebdev-klinkstatus'
	         'kdewebdev-kommander')
	replaces=('kdewebdev')
	conflicts=('kdewebdev')
}
