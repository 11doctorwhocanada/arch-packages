# $Id$
# Maintainer: Dan McGee <dan@archlinux.org>
# Contributor: Sebastien Binet <binet@lblbox>

pkgname=('python-pip' 'python2-pip')
pkgver=6.0.8
pkgrel=1
pkgdesc="The PyPA recommended tool for installing Python packages"
url="https://pip.pypa.io/"
arch=('any')
license=('MIT')
makedepends=('python' 'python-setuptools' 'python2' 'python2-setuptools')
source=(http://pypi.python.org/packages/source/p/pip/pip-${pkgver}.tar.gz)
md5sums=('2332e6f97e75ded3bddde0ced01dbda3')
sha256sums=('0d58487a1b7f5be2e5e965c11afbea1dc44ecec8069de03491a4d0d6c85f4551')

package_python-pip() {
  depends=('python' 'python-setuptools')

  cd "$srcdir/pip-$pkgver"
  python setup.py build
  python setup.py install --prefix=/usr --root="$pkgdir"

  install -D -m644 LICENSE.txt \
	  "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

package_python2-pip() {
  depends=('python2' 'python2-setuptools')
  conflicts=('python-pyinstall')
  replaces=('python-pyinstall')

  cd "$srcdir/pip-$pkgver"
  python2 setup.py build
  python2 setup.py install --prefix=/usr --root="$pkgdir"
  
  mv "$pkgdir/usr/bin/pip" "$pkgdir/usr/bin/pip2"
  sed -i "s|#!/usr/bin/env python$|#!/usr/bin/env python2|" \
    ${pkgdir}/usr/lib/python2.7/site-packages/pip/__init__.py
  python2 -m compileall ${pkgdir}/usr/lib/python2.7/site-packages/pip/__init__.py
  
  install -D -m644 LICENSE.txt \
	  "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
