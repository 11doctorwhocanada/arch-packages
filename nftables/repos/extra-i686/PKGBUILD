# $Id$
# Maintainer: Sébastien "Seblu" Luttringer <seblu@archlinux.org>

pkgname=nftables
epoch=1
pkgver=0.4
pkgrel=3
pkgdesc='Netfilter tables userspace tools'
arch=('i686' 'x86_64')
url='http://netfilter.org/projects/nftables/'
license=('GPL2')
depends=('libmnl' 'libnftnl' 'gmp' 'readline' 'ncurses')
makedepends=('docbook2x')
backup=('etc/nftables.conf')
validpgpkeys=('57FF5E9C9AA67A860B557AF7A4111F89BB5F58CC') # Netfilter Core Team
source=("http://netfilter.org/projects/nftables/files/nftables-$pkgver.tar.bz2"{,.sig}
        'nftables.conf'
        'nftables.service'
        'nftables-reload')
sha1sums=('c557c710510c59e4280d271e5b7232af7ba3fbb7'
          'SKIP'
          'a7146fad414f9e827e2e83b630308890c876b80d'
          '65833b9c5b777cfb3a0776060c569a727ce6f460'
          'd9f40e751b44dd9dc9fdb3b7eba3cc0a9b7e1b01')

build() {
  cd $pkgname-$pkgver
  ./configure \
    --prefix=/usr \
    --sbindir=/usr/bin \
    --sysconfdir=/usr/share \
    CONFIG_MAN=y DB2MAN=docbook2man
  make
}

package() {
  pushd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
  popd
  # basic safe firewall config
  install -Dm644 nftables.conf "$pkgdir/etc/nftables.conf"
  # systemd
  install -Dm644 nftables.service "$pkgdir/usr/lib/systemd/system/nftables.service"
  install -Dm755 nftables-reload "$pkgdir/usr/lib/systemd/scripts/nftables-reload"
}

# vim:set ts=2 sw=2 et:
