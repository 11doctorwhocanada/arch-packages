# $Id$
# Maintainer: Felix Yan <felixonmars@archlinux.org>

pkgname=('python-mako' 'python2-mako')
pkgver=1.0.1
pkgrel=1
pkgdesc="Hyperfast and lightweight templating for the Python2 platform"
arch=('any')
url="http://www.makotemplates.org/"
license=('MIT')
makedepends=('python-setuptools' 'python2-setuptools' 'python-markupsafe' 'python2-markupsafe'
             'python-beaker' 'python2-beaker')
checkdepends=('python-nose' 'python2-nose' 'python-mock' 'python2-mock')
source=(https://pypi.python.org/packages/source/M/Mako/Mako-$pkgver.tar.gz{,.asc})
sha1sums=('00ef43d5722c5407e5d64047ef4e3218006d741c'
          'SKIP')
validpgpkeys=('83AF7ACE251C13E6BB7DEFBD330239C1C4DAFEE1')  # Michael Bayer

prepare() {
    cp -a Mako-$pkgver python2-Mako-$pkgver
}

build() {
    cd Mako-$pkgver
    python3 setup.py build

    cd ../python2-Mako-$pkgver
    python2 setup.py build
}

check() {
    cd Mako-$pkgver
    python3 setup.py test

    cd ../python2-Mako-$pkgver
    python2 setup.py test
}

package_python-mako() {
    depends=('python-markupsafe' 'python-beaker')

    cd Mako-$pkgver
    python3 setup.py install --root="$pkgdir" --optimize=1
    install -D LICENSE "$pkgdir/usr/share/licenses/python-mako/COPYING"
}

package_python2-mako() {
    depends=('python2-markupsafe' 'python2-beaker')

    cd python2-Mako-$pkgver
    python2 setup.py install --root="$pkgdir" --optimize=1
    install -D LICENSE "$pkgdir/usr/share/licenses/python2-mako/COPYING"

    mv "$pkgdir/usr/bin/mako-render" "$pkgdir/usr/bin/mako-render2"
}
