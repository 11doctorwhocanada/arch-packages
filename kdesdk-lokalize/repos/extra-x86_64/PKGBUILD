# $Id$
# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>

pkgname=kdesdk-lokalize
pkgver=14.12.2
pkgrel=1
pkgdesc='Computer-Aided Translation System'
url='http://kde.org/applications/development/lokalize/'
arch=('i686' 'x86_64')
license=('GPL' 'LGPL' 'FDL')
groups=('kde' 'kdesdk')
depends=('kdebase-runtime' 'kdebindings-python2' 'kdesdk-strigi-analyzers')
makedepends=('cmake' 'automoc4')
optdepends=('translate-toolkit: enable extra scripts'
            'kdebindings-kross-python: enable python scripts')
install=${pkgname}.install
source=("http://download.kde.org/stable/applications/${pkgver}/src/lokalize-${pkgver}.tar.xz"
        'fix-python2-path.patch')
sha1sums=('951d895cf8fd69bce7cbe7c040a361c91f78012a'
          '07db42bbcae402c292eebf002e549b04162621c5')

prepare() {
  mkdir build

  cd lokalize-${pkgver}
  patch -p2 -i "${srcdir}"/fix-python2-path.patch
}

build() {
  cd build
  cmake ../lokalize-${pkgver} \
    -DCMAKE_BUILD_TYPE=Release \
    -DKDE4_BUILD_TESTS=OFF \
    -DCMAKE_INSTALL_PREFIX=/usr
  make
}

package() {
  cd build
  make DESTDIR=$pkgdir install

  # Fix python 2 path
  sed -i 's|#!/usr/bin/env python|#!/usr/bin/env python2|' \
    "${pkgdir}"/usr/share/apps/lokalize/scripts/odf/xliffmerge.py
}
