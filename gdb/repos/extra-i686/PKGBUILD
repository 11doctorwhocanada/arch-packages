# $Id$
# Maintainer: Allan McRae <allan@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgname=gdb
pkgver=7.8.2
pkgrel=1
pkgdesc='The GNU Debugger'
arch=('i686' 'x86_64')
url="http://www.gnu.org/software/gdb/"
license=('GPL3')
depends=('ncurses' 'expat' 'python2' 'xz' 'guile')
makedepends=('texinfo')
backup=('etc/gdb/gdbinit')
install=gdb.install
source=(http://ftp.gnu.org/gnu/gdb/${pkgname}-${pkgver}.tar.xz{,.sig})
md5sums=('a80cf252ed2e775d4e4533341bbf2459'
         'SKIP')
validpgpkeys=('F40ADB902B24264AA42E50BF92EDB04BFF325CF3') # Joel Brobecker

prepare() {
  cd gdb-$pkgver

  # hack! - libiberty configure tests for header files using "$CPP $CPPFLAGS"
  sed -i "/ac_cpp=/s/\$CPPFLAGS/\$CPPFLAGS -O2/" libiberty/configure
}

build() {
  cd gdb-$pkgver
  
  # guile support has a severe bug https://sourceware.org/bugzilla/show_bug.cgi?id=17247
  ./configure --prefix=/usr --disable-nls \
    --with-system-readline \
    --with-python=/usr/bin/python2 \
    --with-system-gdbinit=/etc/gdb/gdbinit
  make
}

package() {
  cd gdb-$pkgver
  make DESTDIR=$pkgdir install

  # install "custom" system gdbinit
  install -dm755 $pkgdir/etc/gdb
  touch $pkgdir/etc/gdb/gdbinit

  # resolve conflicts with binutils
  rm $pkgdir/usr/include/{ansidecl,bfd,bfdlink,dis-asm,symcat}.h
  rm $pkgdir/usr/share/info/{bfd,configure,standards}.info
  rm $pkgdir/usr/lib/{libbfd,libopcodes}.a
}
