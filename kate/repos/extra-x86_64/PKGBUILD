# $Id$
# Maintainer: Felix Yan <felixonmars@archlinux.org>
# Contributor: Andrea Scarpino <andrea@archlinux.org>

pkgbase=kate
pkgname=('kwrite'
         'kate')
pkgver=14.12.2
pkgrel=2
arch=('i686' 'x86_64')
license=('GPL' 'LGPL' 'FDL')
depends=('knewstuff' 'ktexteditor' 'threadweaver' 'kded' 'kitemmodels')
makedepends=('extra-cmake-modules' 'kdoctools' 'python' 'plasma-framework')
optdepends=('kio-extras: extra protocols support (sftp, fish and more)')
source=("http://download.kde.org/stable/applications/${pkgver}/src/${pkgbase}-${pkgver}.tar.xz"
        fix-dolphin-hang.patch)
sha1sums=('bc3a5b1213fbe96756261daadf9ff8f94e74fd9c'
          '4b15188df9d6cce000022f6f7969bf00ebe702af')

prepare() {
  mkdir build

  # FS#43513 / https://bugs.kde.org/show_bug.cgi?id=339094
  cd ${pkgbase}-${pkgver}
  patch -p1 -i ../fix-dolphin-hang.patch
}

build() {
  cd build
  cmake ../${pkgbase}-${pkgver} \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DSYSCONF_INSTALL_DIR=/etc \
    -DKDE_INSTALL_USE_QT_SYS_PATHS=ON \
    -DBUILD_TESTING=OFF
  make
}

package_kwrite() {
  pkgdesc="Text Editor"
  groups=('kde' 'kdebase')
  url="http://www.kde.org/applications/utilities/kwrite/"
  install='kwrite.install'
  replaces=('kdebase-kwrite')
  conflicts=('kdebase-kwrite')
  provides=('kdebase-kwrite')

  cd build/kwrite
  make DESTDIR="${pkgdir}" install

  cd ../../build/doc/kwrite
  make DESTDIR="${pkgdir}" install
}

package_kate() {
  pkgdesc="Advanced Text Editor"
  groups=('kde' 'kdesdk')
  url="http://www.kde.org/applications/utilities/kate/"
  install='kate.install'
  replaces=('kdesdk-kate')
  conflicts=('kdesdk-kate')
  provides=('kdesdk-kate')
  optdepends=('konsole: open a terminal in Kate')

  cd build/kate
  make DESTDIR="${pkgdir}" install

  cd ../../build/doc/kate
  make DESTDIR="${pkgdir}" install

  cd ../../../build/addons
  make DESTDIR="${pkgdir}" install
}
