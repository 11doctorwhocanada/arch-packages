# $Id$
# Maintainer : Ionut Biru <ibiru@archlinux.org>
# Contributor: Adam Hani Schakaki <krzd@krzd.net>

pkgname=caribou
pkgver=0.4.16
pkgrel=1
pkgdesc="A text entry and UI navigation application (on-screen keyboard)"
arch=('i686' 'x86_64')
url="http://live.gnome.org/Caribou"
license=(LGPL)
depends=(at-spi2-atk python2-atspi python2-gobject gtk3 libxklavier libgee clutter dconf)
makedepends=(intltool docbook-xsl gtk2)
install=caribou.install
options=(!emptydirs)
source=(http://ftp.gnome.org/pub/GNOME/sources/$pkgname/${pkgver:0:3}/$pkgname-$pkgver.tar.xz)
sha256sums=('8e70090f9cf64e3b42f6995e3228ab1f38a438687d13e2aa9497925a2a6b1d32')

prepare() {
  cd $pkgname-$pkgver
  sed -i s'|#!/usr/bin/python|#!/usr/bin/python2|'g tools/{fix_gir,make_schema}.py
}

build() {
  cd $pkgname-$pkgver
  export PYTHON=/usr/bin/python2
  ./configure --prefix=/usr --sysconfdir=/etc \
    --libexecdir=/usr/lib/$pkgname \
    --disable-static \
    --disable-schemas-compile

  # https://bugzilla.gnome.org/show_bug.cgi?id=655517
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool

  make
}

check() {
  cd $pkgname-$pkgver
  make -k check
}

package() {
  cd $pkgname-$pkgver
  make DESTDIR="$pkgdir" install
}

# vim:set ts=2 sw=2 et:
